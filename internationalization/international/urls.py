from django.conf.urls.static import static
from django.conf.urls import patterns, url, include
from django.conf import settings
from django.views.generic import TemplateView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',


  url(r'^/?$','international.views.index', name='index'),
  url(r'^/about/?$','international.views.about', name='about'),
  url(r'^/services/?$','international.views.services', name='services'),
  url(r'^/contact/?$','international.views.contact', name='contact'),
  
	
)
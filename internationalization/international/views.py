# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate, login as django_login
from django.contrib.auth.decorators import login_required

from django.utils.translation import ugettext as _
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic.simple import direct_to_template



def index(request):  
  hi = _(u'Hi my name is ') + u'Eduardo Lujan' + _(u' this is my personal site')
  text_in_paragraph = _(u""" This is an example with a long string in django please check this, you can 
    use a doc string to use a very long string this is in unicode ... regards
    """)
  text_in_title_h1 = _(u'Text in tittle in tag h1')
  return render_to_response(
    'index.html', 
    {
      'hello_string':hi,
      'text_in_p':text_in_paragraph,
      'text_in_title_h1':text_in_title_h1
    },
    context_instance=RequestContext(request)
  )
